import { Component, OnInit, Input } from '@angular/core';
import { Empresa } from '../empresas.model';

@Component({
  selector: 'app-empresa-descricao',
  templateUrl: './empresa-descricao.component.html',
  styleUrls: ['./empresa-descricao.component.css']
})
export class EmpresaDescricaoComponent implements OnInit {

  @Input() empresa: Empresa
  private id: number;
  
  constructor() { }

  ngOnInit() {
    this.empresa = new Empresa();
  }
}
