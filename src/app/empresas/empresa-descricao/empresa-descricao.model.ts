export interface Enterprises {
    enterprises: Enterprise[];
}
export interface Enterprise {
id: number;
enterprise_name: string;
photo: any;
description: string;
city: string;
country: string;
value: number;
enterprise_type: EnterpriseType;
}

export interface EnterpriseType {
id: number;
enterprise_type_name: string;
}
export interface EnterpriseUn {
enterprise: DadosUn;
success: boolean;
}
export interface DadosUn {
id: number;
enterprise_name: string;
description: string;
photo?: any;
value: number;
city: string;
country: string;
enterprise_type: EnterpriseType;
}