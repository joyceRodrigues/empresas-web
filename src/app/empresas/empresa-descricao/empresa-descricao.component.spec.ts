import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EmpresaDescricaoComponent } from './empresa-descricao.component';

describe('EmpresaDescricaoComponent', () => {
  let component: EmpresaDescricaoComponent;
  let fixture: ComponentFixture<EmpresaDescricaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaDescricaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaDescricaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
