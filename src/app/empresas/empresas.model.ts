export class Empresa {
    public id: number
    public empresa_nome: string
    public email_empresa: string
    public facebook: string
    public twitter: string
    public linkedin: string
    public objeti: string
    public phone: string
    public own_enterprise: boolean
    public foto: string
    public descricao: string
    public city: string
    public pais: string
    public value: number
    public share_price: 5000
    public empresa_tipo: {
      id: number
      empresa_tipo_nome: string
    };
  }