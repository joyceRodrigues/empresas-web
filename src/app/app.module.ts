import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { MaterializeModule } from 'angular2-materialize';
import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { EmpresaListaComponent } from './empresas/empresa-lista/empresa-lista.component';
import { EmpresaDescricaoComponent } from './empresas/empresa-descricao/empresa-descricao.component';
import { EmpresasService } from './empresas/empresa-lista/empresa-lista.service';
import { CommonModule } from '@angular/common';
import { EmpresasComponent } from './empresas/empresas.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmpresaListaComponent,
    EmpresaDescricaoComponent,
    EmpresasComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [LoginService, CookieService, EmpresasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
