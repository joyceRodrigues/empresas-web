import { Component } from '@angular/core';
import {EmpresaListaComponent} from './empresas/empresa-lista/empresa-lista.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmpresaListaComponent]
})
export class AppComponent {
  title = 'empresas-web';
}
